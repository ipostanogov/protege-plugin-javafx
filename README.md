# protege-plugin-javafx

This repository contains modified example code from https://github.com/protegeproject/protege-plugin-examples

## build

mvn clean package

## run

* Add `protege-plugin-javafx-1.0-SNAPSHOT.jar` to the `plugins` folder of the latest [Protege distribution](http://protege.stanford.edu/products.php#desktop-protege)
* Launch Protege
* Open some ontology
* Choose Window -> Tabs -> Example Tab (once)
* Try to move some nodes