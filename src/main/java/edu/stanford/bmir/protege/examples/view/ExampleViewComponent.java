package edu.stanford.bmir.protege.examples.view;

import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import org.apache.log4j.Logger;
import org.osgi.framework.FrameworkUtil;
import org.protege.editor.owl.ui.view.AbstractOWLViewComponent;
import org.semanticweb.owlapi.model.OWLClass;

import java.awt.*;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.Formatter;
import java.util.Random;

public class ExampleViewComponent extends AbstractOWLViewComponent {
    private static final long serialVersionUID = -4515710047558710080L;
    private static final Logger log = Logger.getLogger(ExampleViewComponent.class);

    @Override
    protected void initialiseOWLView() throws Exception {
        new org.protege.editor.owl.ui.OWLWorkspaceViewsTab();
        Random ran = new Random();
        StringBuilder sb = new StringBuilder();
        Formatter formatter = new Formatter(sb);
        sb.append("[");
        int i = 0;
        for (OWLClass a : getOWLEditorKit().getModelManager().getActiveOntology().getClassesInSignature()) {

            formatter.format("{\"title\": \"%s\", \"id\": %d, \"x\": %d, \"y\": %d },", a.getIRI().getRemainder().isPresent() ? a.getIRI().getRemainder().get() : a.getIRI(), i++, ran.nextInt(1000), ran.nextInt(500));
        }
        sb.append("]");
        setLayout(new BorderLayout());
        JFXPanel jfxPanel = new JFXPanel();
        URL path = FrameworkUtil.getBundle(getClass()).getBundleContext().getBundle().getEntry("6863459.htm");
        Platform.runLater(() -> jfxPanel.setScene(new Scene(new Browser(path, sb.toString()), 750, 500, Color.web("#666970"))));
        add(jfxPanel, BorderLayout.CENTER);
        log.info("Example View Component initialized");
    }

    //    @Override
    protected void disposeOWLView() {

    }
}

class Browser extends Region {

    private final WebView browser = new WebView();
    private final WebEngine webEngine = browser.getEngine();

    Browser(URL path, String initGraphArgs) {
        //apply the styles
        getStyleClass().add("browser");
        // load the web page
//        String url = getClass().getResource("index.html").toExternalForm();
//        System.out.println(url);
        webEngine.load(path.toExternalForm());
//        webEngine.load("https://raw.githubusercontent.com/cjrd/directed-graph-creator/master/index.html");
        webEngine.documentProperty().addListener((prop, oldDoc, newDoc) -> {
//            enableFirebug(webEngine);
//            PrintWriter writer = null;
//            try {
//                writer = new PrintWriter("the-file-name.txt", "UTF-8");
//                writer.println(initGraphArgs);
//                writer.close();
//            } catch (FileNotFoundException | UnsupportedEncodingException e) {
//                e.printStackTrace();
//            }
//            System.out.println(initGraphArgs);
            webEngine.executeScript("initGraph(" + initGraphArgs + ")");
        });
        //add the web view to the scene
        getChildren().add(browser);

    }

    private static void enableFirebug(final WebEngine engine) {
        engine.executeScript("if (!document.getElementById('FirebugLite')){E = document['createElement' + 'NS'] && document.documentElement.namespaceURI;E = E ? document['createElement' + 'NS'](E, 'script') : document['createElement']('script');E['setAttribute']('id', 'FirebugLite');E['setAttribute']('src', 'https://getfirebug.com/' + 'firebug-lite.js' + '#startOpened');E['setAttribute']('FirebugLite', '4');(document['getElementsByTagName']('head')[0] || document['getElementsByTagName']('body')[0]).appendChild(E);E = new Image;E['setAttribute']('src', 'https://getfirebug.com/' + '#startOpened');}");
    }

    @Override
    protected void layoutChildren() {
        double w = getWidth();
        double h = getHeight();
        layoutInArea(browser, 0, 0, w, h, 0, HPos.CENTER, VPos.CENTER);
    }

    @Override
    protected double computePrefWidth(double height) {
        return 750;
    }

    @Override
    protected double computePrefHeight(double width) {
        return 500;
    }
}